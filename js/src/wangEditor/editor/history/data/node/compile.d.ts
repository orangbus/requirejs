/**
 * @description 数据整理
 * @author fangzhicong
 */
import { Compile } from '../type';
/**
 * 将 MutationRecord 转换成自定义格式的数据
 */
export default function compile(data: MutationRecord[]): Compile[];
