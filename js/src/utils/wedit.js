define(["wangEditor"],function(E) {
    let create = function(element,uploadUrl) {
        let imageNumber = 1;
        let uploadImgAccept = ['jpg', 'jpeg', 'png', 'gif', 'bmp'];
        let imageSize = 2 * 1024 * 1024;// 2M,

        let editor = new E(element);
        // 配置 server 接口地址
        editor.config.uploadImgServer = uploadUrl;
        //限制大小
        editor.config.uploadImgMaxSize = imageSize;
        // 限制类型
        editor.config.uploadImgAccept = uploadImgAccept;
        //限制一次最多能传几张图片
        editor.config.uploadImgMaxLength = imageNumber // 一次最多上传 5 个图片
        editor.create();
        return editor;
    }
    return {
        create
    }
});