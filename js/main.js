require.config({
    baseUrl: 'js/src/',
    paths: {
        css: 'css.min',
        jquery : "jquery/jquery-3.5.1.min",
        bootstrap4: "bootstrap4/bootstrap.min",
        popper: "bootstrap4/popper.min",
        vue: 'vue/vue',
        axios: 'vue/axios.min',
        wangEditor : 'wangEditor/wangEditor.min',
        notyf :'notyf/notyf.min',
        clipboard: 'clipboard.min',
        magnify: 'magnify/jquery.magnify', //图片放大插件
        wxjdk: 'http://res.wx.qq.com/open/js/jweixin-1.6.0',
        wxShare: "../app/wxShare",  //微信分享
        layui: "layui/layui/layui",
        layuiCss: "layui/layui/layui",
        layer: 'layer/layer',
        sweetAlert: "sweetalert2/sweetalert2", //https://sweetalert2.github.io/
        sweetAlert2: "https://cdn.bootcdn.net/ajax/libs/limonte-sweetalert2/10.9.0/sweetalert2.all.min",
        dropzone: 'dropzone/dropzone',
        videojs: "videojs/video",
        videojsIe: "videojs-ie8.min",
        validate: 'jquery-validate/jquery.validate.min',
	    dayjs: "dayjs/dayjs.min",
	    aos: "aos/aos", // https://michalsnik.github.io/aos/
	    magicGrid: "MagicGrid/magic-grid.min", // https://github.com/e-oj/Magic-Grid
	    voca: "voca/voca.min", //https://vocajs.com/
	    qrCode: 'qrCode/qrcode.min', //https://github.com/davidshimjs/qrcodejs/
	    cookie: 'https://cdn.jsdelivr.net/npm/js-cookie@rc/dist/js.cookie.min', //https://github.com/js-cookie/js-cookie
        cursor: 'cursor/cursor.min',//点击特效，直接引入即可
        fancybox: "fancybox/jquery.fancybox.min", //http://fancyapps.com/fancybox/3/ (图片放大)
        lightgallery: 'lightgallery/js/lightgallery.min', // https://sachinchoolur.github.io/lightgallery.js/ (图片放大)
        lightgalleryZoom: "lightgallery/js/lg-zoom.min", //lightgallery插件
        bigimage: "lightgallery/js/lg-fullscreen.min", //lightgallery插件
        // 在线markdown
        vditor: "vditor/index.min",
        // 时间选择器: http://www.htmleaf.com/jQuery/Calendar-Date-Time-picker/201906145692.html
        DatePicker: 'DatePicker/rolldate.min', //  https://github.com/weijhfly/rolldate
        // 图片方法:https://fengyuanchen.github.io/viewerjs/
        viewer: "viewer/viewer.min",
        // 好看的弹窗：https://gitee.com/sywlgzs/hsycmsAlert
        hsycms:"hsycms/hsycmsAlert.min",

        // 自定义函数
        editor: "utils/wedit",
        // tinymce 编辑器
        tinymce: "tinymce/tinymce.min",
        //tinymce 插件
        // tinymceZhCn: "tinymce/langs/zh_CN"
    },
    map:{
        '*': {
            'popper.js': 'popper',
            'videojs-ie8.min': 'videojsIe',
        }
    },
    shim:{
        notyf: ['css!notyf'], //加载依赖的css文件
        magnify: ["jquery"],
        sweetAlert: ["css!sweetalert2/sweetalert2"],
        sweetAlert2: ["css!https://cdn.bootcdn.net/ajax/libs/limonte-sweetalert2/10.9.0/sweetalert2.min"],
        layuiCss: ["css!/js/libs/layui/layui/css/layui"], //layui js 可以不依赖css样式
        dropzone: ["css!dropzone"],
        bootstrap4:['jquery',"css!bootstrap4"],
        videojs: ['css!videojs'],
        validate: ["jquery-validate/localization/messages_zh"],
	    aos:["css!aos"],
        fancybox: ["css!fancybox","jquery"],
        cursor: ['jquery'],
        bigimage:["css!../src/lightgallery/css/lightgallery","lightgallery","lightgalleryZoom"], //需要的插件可以按需引入
        vditor: ["jquery","css!../src/vditor/index"],
        viewer:["jquery","css!../src/viewer/viewer.min"],
        hsycms:["jquery","css!../src/hsycms/hsycmsAlert"],
        tinymce: ["jquery"]
    }
});